using MoreMountains.InfiniteRunnerEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SongManager : MonoBehaviour
{
    public List<AudioClip> listSong;
    public Dropdown dropdown;
    public System.Action<AudioClip> onChangeSong;
    public void OnSongSelected(int index)
    {
        var song = listSong[index];
        onChangeSong?.Invoke(song);
        GameManager.Instance.UnPause();

    }
    private void Start()
    {
        dropdown.options.Clear();
        foreach(var song in listSong)
        {
            var option = new List<Dropdown.OptionData>();
            option.Add(new Dropdown.OptionData(song.name));
            dropdown.AddOptions(option);
        }
    }
}
