using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhythmDetector : MonoBehaviour
{
    public AudioSource audioSource;
    public enum Channels
    {
        n512,
        n1024,
        n2048,
        n4096,
        n8192
    }

    ;

    public Channels channels = Channels.n4096;
    public FFTWindow method = FFTWindow.Blackman;
    int channelValue = 2048;
	[Header("RHYTHM DETECTION [Experimental]")]
	public ParticleSystem rhythmParticleSystem;

	public bool autoRhythmParticles = true;

	[Range(0f, 100f)]
	public float rhythmSensibility = 30;

	// Rhythm Minimum Sensibility. This don't need to change, use Rhythm Sensibility instead. Recommended: 1.5
	const float minRhythmSensibility = 1.5f;

	[Range(1, 150)]
	public int amountToEmit = 100;

	[Range(0.01f, 1f)]
	public float rhythmParticlesMaxInterval = 0.25f;

	float remainingRhythmParticlesTime;
	bool rhythmSurpassed = false;

	[Header("BASS / MIRROR SETTINGS")]
	[Range(1f, 300f)]
	public float bassSensibility = 60f;
	[Range(0.5f, 2f)]
	public float bassHeight = 1.5f;
	[Range(1, 5)]
	public int bassHorizontalScale = 1;
	[Range(0, 256)]
	public int bassOffset = 0;

	[Header("TREBLE SETTINGS")]
	[Range(1f, 300f)]
	public float trebleSensibility = 120f;
	[Range(0.5f, 2f)]
	public float trebleHeight = 1.35f;
	[Range(1, 5)]
	public int trebleHorizontalScale = 3;
	[Range(0, 256)]
	public int trebleOffset = 40;
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
