using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventChecker : MonoBehaviour
{
    public float setStrenght;
    public float minStrenght;
    public int rank;
    public int minRank;
    public OnsetType selectType;
    public void OnBeat()
    {
        Debug.LogError("onBeat");
    }
    public void OnSet(OnsetType onSetType,Onset onset)
    {
        if (onset.rank < minRank || onset.strength < minStrenght) return;
        if(onSetType != selectType)
        {
            return;
        }
        setStrenght = onset.strength; 
        rank = onset.rank;
        Debug.LogError($"OnSet: {onSetType}");
    }
    public void OnSubBeat()
    {
        Debug.LogError("OnSubBeat");
    }
    public void OnChange()
    {
        //Debug.LogError("OnChange");
    }
}
