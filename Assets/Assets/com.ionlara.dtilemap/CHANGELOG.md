# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2020-06-18

### This is the first release of *\<3DTilemap\>*.

This is the first release of the 3D Tilemap package. It includes tools to create levels easily, by creating a grid
and letting you place objects or "Tiles" just by cicking. It also allows the user to add new objects as Tiles and 
save their custom Tilesets to use them later. An extra feature included lets the user "bake" several objects into 
one single mesh, and save the baked mesh in the assets.

## [0.1.1] - 2015-12-03
### Fixed
- Fixed paths in editor script that broke the tool.
