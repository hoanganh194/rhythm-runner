#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpawnManager))]
public class SpawnManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var spawnner = target as SpawnManager;
        if (GUILayout.Button("Spawn"))
        {
            spawnner.Spawn();
        }
    }
}
#endif
