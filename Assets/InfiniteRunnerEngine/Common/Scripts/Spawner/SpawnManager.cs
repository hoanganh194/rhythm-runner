using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.InfiniteRunnerEngine;
using MoreMountains.Tools;

public class SpawnManager : MonoBehaviour, MMEventListener<MMGameEvent>
{
    public List<DistanceSpawner> listSpawner;
    public int indexCall = 1;
    public Vector3 spawnPosA = new Vector3(130, -3.1f, 3);
    public Vector3 spawnPosB= new Vector3(130, -3.1f, 0);
    public Vector3 spawnPosC = new Vector3(130, -3.1f, -3);
    public float timeReach = 3f;
    void Start()
    {
        this.MMEventStartListening<MMGameEvent>();
    }
    public void OnMMEvent(MMGameEvent eventType)
    {
        switch (eventType.EventName)
        {
            case "LifeLost":
                DestroyAllSpawner();
                break;

        }
    }
    public void DestroyAllSpawner()
    {
        listSpawner[0].DestroyPool();
    }
    bool first;
    public void Spawn()
    {
        GameObject gameObject = null; 
        if (indexCall == 3)
        {
            indexCall = 2;
        }
        else
        {
            indexCall = 3;
        }
        switch (indexCall)
        {
            case 2:
                gameObject = listSpawner[0].Spawn(spawnPosB);
                break;
            case 3:
                gameObject = listSpawner[0].Spawn(spawnPosC);
                break;
        }
      

        if (!first && gameObject !=null)
        {
            first = true;
            Debug.LogError(Time.time);
        }
       
    }


}
