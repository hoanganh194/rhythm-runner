using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.InfiniteRunnerEngine;

public class BeatDetector : MonoBehaviour,MMEventListener<MMGameEvent>
{
    public AudioClip songPlay;
    public SongManager songSelector;
    public LevelSelector levelSelector;
    public LevelSelector endSelector;
    public AudioSource audioPlayer;
    public AudioSource audioPlayerAhead;
    public RhythmVisualizatorPro rhythmDetector;
    //public BasicController baseController;
    public SpawnManager spawnner;
    public int maxBarrier =2;

    public float minInterval = 1f;
    public float offset = 3f;
    public float offsetMaxInterval = 0.1f;
    private int bpm;
    private bool started = false;
    private bool isPaused = false;
    public float offsetDifferent = 0;
    public float maxMagnitude = 5;
    public float timeBeat;
    public float timeRthytm;
    private int maxRandom;
    private bool firstBeatCall;

    private float startPlayTime;
    private float aheadPlayTime;

    private System.Action onSongComplete;
    IEnumerator songUpdateStatus;
    void Start()
    {
      
        rhythmDetector.onBeatDetected += RhythmDetected;
        rhythmDetector.rhythmParticlesMaxInterval = 0.5f;
        this.MMEventStartListening<MMGameEvent>();
        //baseController.audioClip = songPlay;
        audioPlayer.clip = songPlay;
        audioPlayerAhead.clip = songPlay;
        var bpm = UniBpmAnalyzer.AnalyzeBpm(songPlay);
        songSelector.onChangeSong += ChangeSong;
        maxRandom = 3;
        levelSelector.onRestartLevel += OnRestart;
        endSelector.onRestartLevel += OnRestart;
    }

    private void ChangeSong(AudioClip song)
    {
        spawnner.DestroyAllSpawner();
        started = false;
        audioPlayer.clip = song;
        audioPlayer.time = 0;
        audioPlayerAhead.clip = song;
        audioPlayerAhead.time = 0;
        var bpm = UniBpmAnalyzer.AnalyzeBpm(songPlay);
        bpm = UniBpmAnalyzer.AnalyzeBpm(audioPlayerAhead.clip);
        while (bpm > 180)
        {
            bpm /= 2;
        }
        rhythmDetector.rhythmParticlesMaxInterval = 60f / bpm * 1.2f;
        PauseSound();
        LevelManager.Instance.ChangeSongAndResetLevel();
        GameManager.Instance.Reset();
        GameManager.Instance.SetLives(3);
    }
    IEnumerator SpawnObject(int bpm)
    {
        Debug.LogError("Call");
        audioPlayerAhead.Play();
        aheadPlayTime = Time.time;
        StartCoroutine(PlayPlayerAfter(offset - offsetMaxInterval));
        var timeForOneBeat = 60f / bpm;
        while (timeForOneBeat < minInterval)
        {
            bpm /= 2;
            timeForOneBeat = 60f / bpm;
        }
        yield return new WaitUntil(() => !firstBeatCall);
        while (true)
        {
            timeBeat = Time.time;
            yield return new WaitForSeconds(timeForOneBeat);
            yield return new WaitUntil(() => !isPaused);
        }

    }
    public void OnRestart()
    {
        spawnner.DestroyAllSpawner();
        started = false;
        audioPlayer.time = 0;
        audioPlayerAhead.time = 0;
        PauseSound();
        LevelManager.Instance.ChangeSongAndResetLevel();
        GameManager.Instance.Reset();
        GameManager.Instance.SetLives(3);
    }
    IEnumerator PlayPlayerAfter(float time)
    {
        yield return new WaitForSeconds(time);
        startPlayTime = Time.time;
        audioPlayer.Play();
        Debug.LogError("Time delay check: " + (startPlayTime - aheadPlayTime));
        if(songUpdateStatus != null)
        {
            StopCoroutine(songUpdateStatus);
        }
        songUpdateStatus = CheckForEndSong();
        StartCoroutine(songUpdateStatus);

    }
    IEnumerator CheckForEndSong()
    {
        while (true)
        {
            yield return null;
           
            if (!isPaused && started)
            {
                if (!audioPlayer.isPlaying)
                {
                    Debug.LogError("Game over in 4s");
                    yield return new WaitForSeconds(4f);
                    if(!isPaused && started && !audioPlayer.isPlaying)
                    {
                        GUIManager.Instance.SetGameOverScreen(true);
                        GameManager.Instance.SetStatus(GameManager.GameStatus.GameOver);
                        MMEventManager.TriggerEvent(new MMGameEvent("GameOver"));
                    }
                }
            }
        }
    }
  
    int currentDouble = 0;
    private void RhythmDetected(float magnitude)
    {
        Debug.LogError("CAll");
        spawnner.Spawn();

        Debug.LogError(magnitude);
        timeRthytm = Time.time;
        offsetDifferent = Mathf.Abs(timeRthytm - timeBeat);
        if (magnitude > maxMagnitude && currentDouble < maxBarrier)
        {
            currentDouble++;
            spawnner.Spawn();
        }
        else
        {
            currentDouble = 0;
        }
    }
    public void OnBeat()
    {
        spawnner.Spawn();
    }


    public void OnMMEvent(MMGameEvent eventType)
    {
        Debug.LogError(eventType.EventName);
        switch (eventType.EventName)
        {
            case "PauseOn":
                PauseSound();
                isPaused = true;
                break;
            case "PauseOff":
                UnpauseSound();
                isPaused = false;
                break;
            case "GameStart":
                if (!started)
                {
                    started = true;
                    isPaused = false;
                    StartCoroutine(SpawnObject(bpm));
                }
                else
                {
                    if (isPaused)
                    {
                        UnpauseSound();
                        isPaused = false;
                    }
                }
                break;
            case "GameOver":
                PauseSound();
                isPaused = true;
                break;
           
        }
    }
    private void PauseSound()
    {
        audioPlayer.Pause();
        audioPlayerAhead.Pause();
    }
    private void UnpauseSound()
    {
        audioPlayer.Play();
        audioPlayerAhead.Play();
    }
}
