using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName ="Song data", menuName = "Song",order = 1)]
public class MySong : ScriptableObject
{
    public string SongName => clip.name;
    public AudioClip clip;
    private void OnValidate()
    {
        name = SongName;
    }
}
